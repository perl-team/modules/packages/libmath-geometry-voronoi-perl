Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Math-Geometry-Voronoi
Source: https://metacpan.org/release/Math-Geometry-Voronoi
Upstream-Contact: Sam Tregar <sam@tregar.com>

Files: *
Copyright: 2008-2009, Sam Tregar <sam@tregar.com>
License: Artistic or GPL-1+

Files: *.c *.h
Copyright: 1987, Alcatel-Lucent, as successor to AT&T Bell Laboratories, Inc.
License: MIT
Comment: Copyright information for these files is missing from the upstream
 package, but could be obtained from the original author, Steve Fortune, via
 email: http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=689653#15

Files: ppport.h
Copyright: 2004-2010, Marcus Holland-Moritz <mhx-cpan@gmx.net>
 2001, Paul Marquess <pmqs@cpan.org> (Version 2.x)
 1999, Kenneth Albanowski <kjahds@kjahds.com> (Version 1.x)
License: Artistic or GPL-1+

Files: debian/*
Copyright: 2013, Florian Schlichting <fsfs@debian.org>
License: Artistic or GPL-1+

License: Artistic
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License, which comes with Perl.
 .
 On Debian systems, the complete text of the Artistic License can be
 found in `/usr/share/common-licenses/Artistic'.

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 1 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-1'.

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
